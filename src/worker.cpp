#include <consts.hpp>
#include <helpers.hpp>

using namespace std;

map<int, string> getFilters(string data)
{
    vector<string> splitedData = split(data, '-', true);
    map<int, string> filters;
    for (int i = 0; i < splitedData.size(); i++)
    {
        int key;
        if (i % 2)
            filters.insert(pair<int, string>(key, splitedData[i]));

        else
            key = splitedData[i] == "Name" ? 0 : splitedData[i] == "Platform" ? 1 : splitedData[i] == "Year" ? 2 : splitedData[i] == "Genre" ? 3 : splitedData[i] == "Publisher" ? 4 : -1;
    }
    return filters;
}

vector<string> getFiles(string data, string dir)
{
    vector<string> splitedData = split(data, '-', true);
    vector<string> files;
    for (string x : splitedData)
        files.push_back("./" + dir + "/" + x);
    return files;
}

void searchAndSend(vector<string> &files, map<int, string> filters)
{
    string matchedGames;
    for (int i = 0; i < files.size(); i++)
    {
        string line;
        vector<string> splitedLine;
        ifstream File(files[i]);
        bool matched;
        while (getline(File, line))
        {
            splitedLine = split(line, '-', true);
            matched = true;
            for (const auto &x : filters)
            {
                if (splitedLine[x.first] != x.second)
                    matched = false;
            }
            if (matched || (filters.lower_bound(0)->second == "none" && splitedLine[9] != "Global_Sales"))
                matchedGames += line + "^";
        }
        File.close();
    }
    ofstream outFile(NAMED_PIPE);
    outFile << matchedGames;
    outFile.close();
}

int main(int argc, char *argv[])
{
    if (argc != 1)
        return 0;
    // Read data from pipe
    int pipe = atoi(argv[0]);
    char input[BUF_SIZE];
    read(pipe, input, BUF_SIZE);
    close(pipe);
    // Find matched data
    vector<string> data = split(input, '^', true);
    vector<string> files = getFiles(data[1], data[0]);
    map<int, string> filters;
    if (data.size() == 3)
        filters = getFilters(data[2]);
    else
        filters = {{0, "none"}};

    searchAndSend(files, filters);
    return 1;
}