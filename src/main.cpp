#include <loadBalancer.hpp>

int main()
{
    std::string cmd;
    while(std::getline(std::cin, cmd)){
        if (cmd == "quit")
            break;
        LoadBalancer loadBalancer(cmd);
        loadBalancer.multiProcSearch();
    }
    return 0;
}