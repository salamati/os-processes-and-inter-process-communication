#include <consts.hpp>
#include <helpers.hpp>

using namespace std;

int sortParam, ascending;

struct Game
{
    string stat;
    float val;
};

bool compare(Game g1, Game g2)
{
    return ascending ? g1.val < g2.val : g1.val > g2.val;
}

string getInput()
{
    string line, in = "";
    bool done = false;
    while (!done)
    {
        ifstream NamedPipe(NAMED_PIPE);
        while (getline(NamedPipe, line))
        {
            in += line;
            if (line.find("done") != std::string::npos)
                done = true;
        }
        NamedPipe.close();
    }
    return in;
}

void sortAndPrint(string input)
{
    vector<string> splitedIn = split(input, '^', false);
    vector<string> sortingVals = split(splitedIn[splitedIn.size() - 1], '-', true);
    sortParam = stoi(sortingVals[1]);
    ascending = stoi(sortingVals[2]);
    vector<Game> games;
    for (int i = 0; i < splitedIn.size() - 1; i++)
        games.insert(games.begin(), {splitedIn[i], stof(split(splitedIn[i], '-', true)[sortParam])});
    // Sort games
    sort(games.begin(), games.end(), compare);
    // Print games
    cout << "Name - Platform - Year - Genre - Publisher - NA_Sales - EU_Sales - JP_Sales - Other_Sales - Global_Sales" << endl;
    for (Game g : games)
        cout << g.stat << endl;
}

int main()
{
    vector<string> splitedLine;
    string input = getInput();
    sortAndPrint(input);
    return 0;
}