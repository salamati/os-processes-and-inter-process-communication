#include <loadBalancer.hpp>

LoadBalancer::LoadBalancer(string cmd)
{
    parseCmd(cmd);
}

void LoadBalancer::parseCmd(string s)
{
    s.erase(remove(s.begin(), s.end(), ' '), s.end());
    string param = "";
    string value = "";
    vector<string> sortParams = {"NA_Sales",
                                 "EU_Sales",
                                 "JP_Sales",
                                 "Other_Sales",
                                 "Global_Sales"};
    ascending = 0;
    sortParam = 9;
    for (char &c : s + '-')
    {
        if (c == '-' || c == '=')
        {
            if (param == "")
            {
                param = value;
                value = "";
                continue;
            }
            else if (find(begin(sortParams), end(sortParams), param) != end(sortParams))
            {
                // Find the index of search parameter
                for (int i = 0; i < sortParams.size(); i++)
                    if (sortParams[i] == param)
                        sortParam = i + 5;
                ascending = value == "ascending";
            }
            else if (param == "processes")
                processNum = stoi(value);
            else if (param == "dir")
                dir = value;
            else
                filters += param + "-" + value + "-";
            param = "";
            value = "";
        }
        else
        {
            value += c;
        }
    }
}

void LoadBalancer::assignFiles()
{
    for (int i = 0; i < processNum; i++)
    {
        procFiles.push_back("");
    }
    int n = 1;
    while (n != FILE_NUM + 1)
    {
        for (int i = 0; i < processNum; i++)
        {
            if (n == FILE_NUM + 1)
                return;
            procFiles[i] += DATASET + to_string(n) + "-";
            n++;
        }
    }
}

void LoadBalancer::multiProcSearch()
{
    assignFiles();
    // Create the named pipe
    // 0666 read & write permission
    unlink(NAMED_PIPE);
    mkfifo(NAMED_PIPE, 0666);

    // Create presenter process
    pid_t pid = fork();
    if (pid < (pid_t)0)
    {
        cout << "Fork failed";
        return;
    }
    else if (pid == (pid_t)0)
    {
        // This is the child process
        execl(PRESENTER_DIR, (char *)NULL);
    }
    else
    {
        // This is the parent process
    }

    for (int i = 0; i < processNum; i++)
    {
        // Create the unnamed pipe
        int unnamedPipe[2];
        if (pipe(unnamedPipe) < 0)
        {
            cout << "Pipe failed";
            return;
        }
        // Create worker process
        pid_t pid = fork();
        if (pid < (pid_t)0)
        {
            // The fork failed
            cout << "Fork failed";
            return;
        }
        else if (pid == (pid_t)0)
        {
            // This is the child process
            close(unnamedPipe[1]);
            char *pipe = (char *)to_string(unnamedPipe[0]).c_str();
            char *args[] = {pipe, NULL};
            execv(WORKER_DIR, args);
        }
        else
        {
            // This is the parent process
            close(unnamedPipe[0]);
            string data = dir + "^" + procFiles[i] + "^" + filters;
            write(unnamedPipe[1], data.c_str(), data.length() + 1);
            close(unnamedPipe[1]);
        }
    }

    // Wait child processes finish
    int stat;
    for (int i = 0; i < processNum; i++)
    {
        wait(&stat);
    }

    // Inform presenter search is done and send sorting values to it
    int fd = open(NAMED_PIPE, O_WRONLY);
    string done = "done-" + to_string(sortParam) + "-" + to_string(ascending);
    write(fd, done.c_str(), done.length() + 1);
    close(fd);

    // Wait presenter process finish
    wait(&stat);
}