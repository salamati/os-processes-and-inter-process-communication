#include <helpers.hpp>

vector<string> split(string s, char delimiter, bool removeSpaces)
{
    string item;
    vector<string> items;
    if (removeSpaces)
        s.erase(remove(s.begin(), s.end(), ' '), s.end());
    stringstream ss(s);
    while (getline(ss, item, delimiter))
    {
        items.push_back(item);
    }
    return items;
}
