#include <bits/stdc++.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>

#define FILE_NUM 17

#define DATASET "dataset"

#define BUF_SIZE 1000

#define NAMED_PIPE "./bin/namedpipe"

#define WORKER_DIR "./bin/worker"

#define WORKER_DIR "./bin/worker"

#define PRESENTER_DIR "./bin/presenter"