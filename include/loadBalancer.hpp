#ifndef _LOADBALANCER_H
#define _LOADBALANCER_H

#include <consts.hpp>

using namespace std;

class LoadBalancer
{
public:
    LoadBalancer(string cmd);
    void multiProcSearch();
    void parseCmd(string s);
    void assignFiles();

private:
    string filters;
    int sortParam;
    int ascending;
    int processNum;
    string dir;
    vector<string> procFiles;
};

#endif