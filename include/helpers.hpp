#ifndef _HELPERS_H
#define _HELPERS_H

#include <bits/stdc++.h>

using namespace std;

vector<string> split(string s, char delimiter, bool removeSpaces);

#endif