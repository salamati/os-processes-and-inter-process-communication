CXX		  := g++
CXX_FLAGS := -std=c++11

BIN		:= bin
SRC		:= src
INCLUDE	:= include

EXECUTABLE	:= main
WORKER	:= worker
PRESENTER := presenter


all: $(BIN)/$(EXECUTABLE) $(BIN)/$(WORKER) $(BIN)/$(PRESENTER)

run: clean all
	clear
	@echo "🚀 Executing..."
	./$(BIN)/$(EXECUTABLE)

$(BIN)/$(EXECUTABLE): $(SRC)/main.cpp $(SRC)/loadBalancer.cpp
	@echo "🚧 Building..."
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) $^ -o $@

$(BIN)/$(WORKER): $(SRC)/worker.cpp $(SRC)/helpers.cpp
	@echo "🚧 Building..."
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) $^ -o $@

$(BIN)/$(PRESENTER): $(SRC)/presenter.cpp $(SRC)/helpers.cpp
	@echo "🚧 Building..."
	$(CXX) $(CXX_FLAGS) -I$(INCLUDE) $^ -o $@

clean:
	@echo "🧹 Clearing..."
	-rm $(BIN)/*
